# encoding: utf-8

import json
import datetime

from flask import request, json, jsonify
from flask.views import MethodView


class IndexView(MethodView):
    def get(self):
        data = dict(
            name='andy',
            addr='深圳',
            dt=datetime.datetime.utcnow()
        )
        return jsonify({'status': True, 'message': data})
