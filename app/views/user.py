# encoding: utf-8


from flask.views import MethodView
from flask import jsonify, request, g
from werkzeug.security import generate_password_hash, check_password_hash

from app.models.user import User, Role
from app.models.base import model_to_dict, model_to_dict_v2
from app.extensions import db
from app.libs.decorators import ObjectMustBeExist, TokenAuthenticate, PaginateSearch


class UserView(MethodView):

    search_fields = ["username"]
    decorators = [TokenAuthenticate(), PaginateSearch(User, search_fields)]

    def get(self):
        users = g.instance.get('queryset')
        count = g.instance.get('count')
        user_list = list()

        # 不需要序列化的字段
        no_serializer_fields = ['id', 'remark', 'update_time', '_password']

        for user in users:
            # 将model对象转换成字典
            user_dict = model_to_dict_v2(user, no_serializer_fields)
            user_dict['create_time'] = str(user_dict['create_time'])

            # 显示角色
            user_dict['role'] = [role.name for role in user.roles]

            user_list.append(user_dict)

        result = dict(result=user_list, count=count)

        return jsonify({'status': True, 'data': user_list})

    def post(self):
        data = request.get_json()

        # 检测数据是否已存在
        user = User.query.filter_by(username=data['username']).first()
        if user:
            return jsonify({'status': False, 'message': 'object exsist'}), 403

        # 通过前端传来的role，得到role对象
        role = data.pop('role')
        role_obj = Role.query.filter(Role.name == role).first()

        user_dict = dict(
            username=data['username'],
            realname=data['realname'],
            email=data['username'] + '@xxoo.com'
        )
        user_dict['password'] = generate_password_hash(data['password'])
        user = User(**user_dict)
        # 将用户添加到角色
        user.roles.append(role_obj)
        user.save()

        return jsonify({'status': True, 'message': 'success'}), 201


class UserDetailView(MethodView):

    # 装饰器，api资源请求时调用
    decorators = [TokenAuthenticate(), ObjectMustBeExist(User)]

    def get(self, id):
        user_dict = model_to_dict(g.instance)
        user_dict['create_time'] = str(user_dict['create_time'])

        return jsonify({'status': True, 'data': user_dict})

    def delete(self, id):
        g.instance.delete()
        return jsonify({'status': True, 'message': 'success'}), 201

    def put(self, id):
        data = request.get_json()
        password = data.pop('password')

        try:
            for k, v in data.items():
                User.query.filter_by(id=id).update({k: v})
            g.instance.save()
        except Exception as e:
            return jsonify({'status': True, 'message': str(e)}), 501

        return jsonify({'status': True, 'message': 'success'}), 201


class RoleView(MethodView):

    search_fields = ["name"]
    decorators = [TokenAuthenticate(), PaginateSearch(Role, search_fields)]

    def get(self):
        roles = g.instance.get('queryset')
        count = g.instance.get('count')
        role_list = list()

        no_serializer_fields = ['id', 'remark', 'update_time']

        for role in roles:
            # 将model对象转换成字典
            role_dict = model_to_dict_v2(role, no_serializer_fields)
            role_dict['create_time'] = str(role_dict['create_time'])

            # 显示role用户成员
            role_dict['users'] = [user.username for user in role.users.all()]
            # print(role.users.all())

            role_list.append(role_dict)

        result = dict(result=role_list, count=count)
        return jsonify({'status': True, 'data': result})

    def post(self):
        data = request.get_json()

        # 检测数据是否已存在
        role = Role.query.filter_by(name=data['name']).first()
        if role:
            return jsonify({'status': False, 'message': 'object exsist'}), 403

        role_dict = dict(
            name=data['name']
        )

        role = Role(**role_dict)
        role.save()

        return jsonify({'status': True, 'message': 'success'}), 201
