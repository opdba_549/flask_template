# encoding: utf-8


from flask_migrate import MigrateCommand
from flask_script import Manager

from app import create_app
from app.models.user import User

# 调用工厂函数实例化app
app = create_app()

# 自定义命令行命令db
manager = Manager(app=app)
manager.add_command('db', MigrateCommand)

# 自定义命令 -- 用于命令行创建管理员账号
# python manage.py create_admin -u admin -p 123456
@manager.option('-u', '--user', dest='user')
@manager.option('-p', '--password', dest='password')
def create_admin(user, password):
    # 创建一个默认的管理员账号
    with app.app_context():
        User.create_admin(user, password)


if __name__ == "__main__":
    manager.run()
