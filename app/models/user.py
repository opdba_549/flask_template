"""用户相关模型
"""

import jwt

from datetime import datetime, timedelta
from calendar import timegm

from flask import current_app, jsonify
from werkzeug.security import generate_password_hash, check_password_hash

from .base import BaseModel
from app.extensions import db
from app.libs.error import AuthenticationError, InvalidTokenError


# 用户和组关联表
user_role_table = db.Table('user_role_table',
                           db.Column('user_id', db.Integer,
                                     db.ForeignKey('user.id')),
                           db.Column('role_id', db.Integer,
                                     db.ForeignKey('role.id'))
                           )


class User(BaseModel):

    __tablename__ = "user"

    username = db.Column(db.String(32), unique=True, comment="用户名")
    realname = db.Column(db.String(64), comment="用户真实姓名或别名")
    email = db.Column(db.String(64), unique=True, comment="邮箱")
    _password = db.Column(db.String(128), comment="密码")
    is_admin = db.Column(db.Boolean, default=False, comment="是否是管理员")
    login_at = db.Column(db.DateTime, comment="登录时间")
    roles = db.relationship(
        'Role', secondary=user_role_table, backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return "<User {}>".format(self.username)
    __repr__ = __str__

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        # 生成加密密码
        self._password = generate_password_hash(password)

    def verify_password(self, password):
        # 检查密码
        return check_password_hash(self.password, password)

    @classmethod
    def authenticate(cls, identifier, password):
        # 认证用户或邮箱
        user = cls.query.filter(
            db.or_(cls.username == identifier, cls.email == identifier)).first()
        if not user or not user.verify_password(password):
            # --- 这里抛出异常可以自定义一个异常类 ---
            raise AuthenticationError(403, 'authentication failed')
        return user

    @classmethod
    def create_admin(cls, username, password):
        # 创建管理员账号admin, 初使密码为admin(在程序app启动时调用)

        # 检查管理员账号是否存在，不存在则创建
        admin = cls.query.filter_by(username=username).first()
        if admin:
            return admin.username
        admin = User(username=username, email='{}@xxoo.com'.format(
            username), is_admin=True)
        # 调用实例属性方法设置密码
        admin.password = password
        admin.save()
        return username, password

    def generate_token(self):
        """ 生成 json web token
        有效期为1天，过期后十分种内可以使用token刷新获取新的token
        """
        # token 过期时间，默认有效期1天
        exp_time = datetime.utcnow() + timedelta(days=1)
        # token 过期10分钟内，可以使用老的token进行刷新，得到新的token
        refresh_exp = timegm(
            (exp_time + timedelta(seconds=60 * 10)).utctimetuple())

        # 生成token
        try:
            payload = {
                'uid': self.id,
                'is_admin': self.is_admin,
                'exp': exp_time,
                'refresh_exp': refresh_exp
            }
            token = jwt.encode(payload, current_app.secret_key,
                               algorithm='HS512').decode('utf-8')
            return token
        except Exception as e:
            return e

    @classmethod
    def verify_token(cls, token, verify_exp=True):
        # 验证token
        now = datetime.utcnow()

        try:
            payload = jwt.decode(
                token, current_app.secret_key, algorithm=['HS512'])
        except jwt.InvalidTokenError as e:
            # raise InvalidTokenError(403, str(e))
            return jsonify({'status': False, 'message': 'Signature has expired'}), 403

        # 如果刷新时间过期，则认为token无效
        if payload['refresh_exp'] < timegm(now.utctimetuple()):
            raise InvalidTokenError(403, 'invalid token')

        user = cls.query.get(payload['uid'])
        if not user:
            raise InvalidTokenError(403, 'user not exist.')
        return user


class Role(BaseModel):

    __tablename__ = "role"

    name = db.Column(db.String(32), unique=True, comment="用户组名")

    def __str__(self):
        return "<Role {}>".format(self.name)
    __repr__ = __str__
