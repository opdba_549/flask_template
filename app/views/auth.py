# encoding: utf-8

from datetime import datetime
from flask import request, jsonify
from flask.views import MethodView

from app.models.user import User


class AuthView(MethodView):
    """用户可以通过用户名或邮箱登录，登录成功后返回用于后续认证的token
    """

    def post(self):
        data = request.get_json()
        if not data:
            return {'ok', False}

        identifier = data.get('identifier')
        password = data.get('password')

        if not identifier or not password:
            return None

        user = User.authenticate(identifier, password)
        user.login_at = datetime.utcnow()
        user.save()

        return jsonify({'status': True, 'token': user.generate_token(), "userid": user.id})
