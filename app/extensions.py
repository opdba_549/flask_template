"""项目所有的第三方扩展包
"""

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_cors import CORS

db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()
cors = CORS()


def init_ext(app):
    db.init_app(app)
    # 解决(RuntimeError: No application found. Either work inside a view function or push an application context.)
    db.app = app
    migrate.init_app(app, db)
    ma.init_app(app)
    cors.init_app(app, supports_credentials=True)
